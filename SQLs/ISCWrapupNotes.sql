SELECT 
	Participants.ConversationId,
	DATETIME(Participants.StartTime, 'localtime') AS StartTime, 
	DATETIME(Participants.EndTime, 'localtime') AS EndTime,
	Participants.Name AS AgentName,
	Participants.QueueId AS QueueId,
	Participants.ParticipantType AS ParticipantType,
	Participants.Address AS AgentAddress,
	Participants.Ani AS CallerAddress,
	Participants.Dnis AS CalledAddress,
	Participants.WrapupCode AS WrapupId,
	Participants.WrapupDuration AS WrapupDuration,
	Participants.WrapupName AS WrapupName,
	Participants.WrapupNotes AS WrapupNotes
FROM Participants
WHERE Participants.WrapupName = 'Other'
ORDER BY StartTime