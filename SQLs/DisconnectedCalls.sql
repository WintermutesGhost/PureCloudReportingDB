SELECT * 
FROM Participants 
WHERE (Participants.ParticipantId IN
		(SELECT Calls.ParticipantId 
		FROM Calls 
		WHERE Calls.DisconnectType = 'client')
	AND
		Participants.Purpose = 'agent'
	AND
		Date(Participants.EndTime) = Date('2017-08-23')
	AND
		Participants.ParticipantId NOT IN
		(SELECT Calls.ParticipantId 
		FROM Calls 
		WHERE Calls.CallId IN 
			(SELECT Segments.CallId
			FROM Segments
			WHERE Segments.Type = 'Interact')
		)
	)
