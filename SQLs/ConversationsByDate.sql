SELECT COUNT(Conversations.ConversationId), date(Conversations.StartTime)
FROM Conversations
WHERE Conversations.ConversationId IN 
	(SELECT Participants.ConversationId
	FROM Participants
	WHERE Participants.QueueId = "db919120-ca72-4c16-89c3-f46a9dfe3291")
GROUP BY date(Conversations.StartTime)
