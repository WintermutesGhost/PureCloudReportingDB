BEGIN TRANSACTION;
CREATE TABLE "Segments" (
	`SegmentId`	INTEGER,
	`CallId`	TEXT,
	`StartTime`	TEXT,
	`EndTime`	TEXT,
	`HowEnded`	TEXT,
	`DisconnectType`	TEXT,
	`Type`	TEXT,
	PRIMARY KEY(`SegmentId`),
	FOREIGN KEY(`CallId`) REFERENCES `Calls`(`CallId`)
);
CREATE TABLE "Participants" (
	`ParticipantId`	TEXT,
	`ConversationId`	TEXT,
	`StartTime`	TEXT,
	`EndTime`	TEXT,
	`Name`	TEXT,
	`UserId`	TEXT,
	`QueueId`	TEXT,
	`Purpose`	TEXT,
	`ParticipantType`	TEXT,
	`Address`	TEXT,
	`Ani`	TEXT,
	`AniName`	TEXT,
	`Dnis`	TEXT,
	`WrapupCode`	INTEGER,
	`WrapupDuration`	INTEGER,
	`WrapupEndTime`	INTEGER,
	`WrapupName`	INTEGER,
	`WrapepNotes`	INTEGER,
	PRIMARY KEY(`ParticipantId`),
	FOREIGN KEY(`ConversationId`) REFERENCES `Conversations`(`ConversationId`)
);
CREATE TABLE "Conversations" (
	`ConversationId`	TEXT,
	`StartTime`	TEXT,
	`EndTime`	TEXT,
	PRIMARY KEY(`ConversationId`)
);
CREATE TABLE "Calls" (
	`CallId`	TEXT,
	`ParticipantId`	TEXT,
	`State`	TEXT,
	`Direction`	TEXT,
	`DisconnectType`	TEXT,
	`ConnectedTime`	TEXT,
	`DisconnectedTime`	TEXT,
	`Provider`	TEXT,
	`DisconnectReasons`	TEXT,
	`ScriptId`	TEXT,
	PRIMARY KEY(`CallId`),
	FOREIGN KEY(`ParticipantId`) REFERENCES `Participants`(`ParticipantId`)
);
COMMIT;
