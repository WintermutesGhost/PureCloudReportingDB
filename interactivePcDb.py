import pctoolkit
import pcconsole
from pctoolkit.analytics import daysInterval
#import pureCloudToDb
import os
import sqlite3
import datetime

print("Please confirm current working directory") #TODO: Make better
print(os.getcwd())
input()

DBNAME = 'pcdb-0.2.db'

SQL_INSERT = "INSERT OR REPLACE INTO {0} VALUES ({1})"

#TODO: Automatically generate table structure
TABLE_STRUCTURE = {
    'Conversations':('ConversationId','StartTime','EndTime'),
    'Participants':('ParticipantId','ConversationId','StartTime','EndTime',
                    'Name','UserId','QueueId','Purpose','ParticipantType',
                    'Address','Ani','AniName','Dnis','WrapupCode',
                    'WrapupDuration','WrapupEndTime','WrapupName',
                    'WrapupNotes'),
    'Calls':('CallId','ParticipantId','State','Direction','DisconnectType',
             'ConnecectedTime','DisconnectedTime','Provider',
             'DisconnectReasons','ScriptId'),
    'Segments':('SegmentId','CallId','StartTime','EndTime','HowEnded',
                'DisconnectType','Type')
    }


def insertQueryBuilder(tableName,columnCount): #No spaces allowed
    cleanTableName = ''.join(c for c in tableName if c.isalnum())
    valuePlaceholders = '?'+(',?'*(columnCount-1))
    query = SQL_INSERT.format(cleanTableName,valuePlaceholders)
    return query

def normalizeConversations(convs):
    nTables = {'Conversations':[],'Participants':[],'Calls':[],'Segments':[]}
    for conv in convs:
        nConv = (
            conv.id,                    # ConversationId
            toLocalTime(conv.start_time),# StartTime
            toLocalTime(conv.end_time)  # EndTime
            )
        deeperTables = normalizeParticipants(conv.participants,conv.id)
        nTables['Conversations'].append(nConv)
        appendTables(nTables,deeperTables)
    return nTables
    
def normalizeParticipants(parts,conversationId):
    nTables = {'Participants':[],'Calls':[],'Segments':[]}
    for part in parts:
        nPart = ( #TODO: Improve how to map table columns to tuple
            part.id,                    # ParticipantId
            conversationId,             # ConversationId
            toLocalTime(part.start_time),# StartTime
            toLocalTime(part.end_time), # EndTime
            part.name,                  # Name
            part.user_id,               # UserId
            part.queue_id,              # QueueId
            part.purpose,               # Purpose
            part.participant_type,      # ParticipantType
            part.address,               # Address
            part.ani,                   # Ani
            part.ani_name,              # AniName
            part.dnis                   # Dnis
            )
        nWrapup = unwrapWrapup(part.wrapup)
        nPart = nPart + nWrapup
        deeperTables = normalizeCalls(part.calls,part.id)
        #TODO: Wrapups table
        nTables['Participants'].append(nPart)
        appendTables(nTables,deeperTables)
    return nTables

def normalizeCalls(calls,participantId):
    nTables = {'Calls':[],'Segments':[]}
    for call in calls:
        nCall = (
            call.id,                            # CallId
            participantId,                      # ParticipantId
            call.state,                         # State
            call.direction,                     # Direction
            call.disconnect_type,               # DisconnectType
            toLocalTime(call.connected_time),   # ConnectedTime
            toLocalTime(call.disconnected_time),# DisconnectedTime
            call.provider,                      # Provider
            None,                               # DisocnnectReasons #TODO
            call.script_id                      # ScriptId
            )
        deeperTables = normalizeSegments(call.segments,call.id)
        nTables['Calls'].append(nCall)
        appendTables(nTables,deeperTables)
    return nTables

def normalizeSegments(segms,callId):
    nTables = {'Segments':[]}
    for segm in segms:
        nSegm = (
            None,                           # SegmentId
            callId,                         # CallId
            toLocalTime(segm.start_time),   # StartTime
            toLocalTime(segm.end_time),     # EndTime
            segm.how_ended,                 # HowEnded
            segm.disconnect_type,           # DisconnectType
            segm.type                       # Type
            )
        nTables['Segments'].append(nSegm)
    return nTables

def toLocalTime(utcTime):
    if utcTime is None:
        return None 
    #return utcTime.replace(tzinfo=datetime.timezone.utc).astimezone(tz=None)
    return utcTime #TODO: Test removing the conversion, SQLITE3 should be able to handle it directly 

def unwrapWrapup(wrapup):
    try:
        wrapup = (
            wrapup.code,
            wrapup.duration_seconds,
            toLocalTime(wrapup.end_time),
            wrapup.name,
            wrapup.notes
            )
    except AttributeError:
        wrapup = (
            None,
            None,
            None,
            None,
            None
            )
    return wrapup
        

def writeTables(tables):
    print("Connecting to database")
    with sqlite3.connect(DBNAME) as connection:
        print("\tConnected.\n")
        for tableName,rows in tables.items():
            columnNames = TABLE_STRUCTURE[tableName]
            print("Writing to table: {0}: {1} columns"
                  .format(tableName,len(columnNames)))
            query = insertQueryBuilder(tableName,len(columnNames))
            uniqueRows = list(set(rows))
            try:
                connection.executemany(query,uniqueRows) # TODO: Uniqueness check before entry?
            except sqlite3.OperationalError:
                print("Database appears to be locked.")
                input("Try again?")
                connection.executemany(query,uniqueRows)
            print("\t{} rows added".format(len(uniqueRows)))
        print("\nAll records written. Commit changes?")
        confirm = input("(y/n)? ")
        if confirm.lower()[0] == 'y':
            print("Writing changes\n")
            connection.commit()
        else:
            print("Reverting changes")
#        connection.close() #Not needed because of WITH
    print("Database connection closed\n")

def appendTables(topTables,bottomTables):
    for tableName,tableValues in topTables.items():
        if tableName in bottomTables:
            topTables[tableName] += bottomTables[tableName]
    return topTables

def retrieveFullConvs(analyticsConvs):
    convIds = [c.conversation_id for c in analyticsConvs]
    fullConvs = []
    processedCount = 0
    convCount = len(convIds)
    processIncrement = 100
    chunkedConvIds = [convIds[x:x+100] for x in range(0,convCount,processIncrement)]
    for convIdsChunk in chunkedConvIds:
        fullConvs += pctoolkit.conversations.getConversationList(convIdsChunk)
        processedCount += processIncrement
        print("\t{0}/{1} complete...".format(processedCount,convCount))
    return fullConvs

def addConversationsInInterval(interval):
    print("Retrieving conversation list for interval")
    analyticsConvs = pctoolkit.analytics.getConversationsInInterval(interval)
    print("\t{0} conversations found\n".format(len(analyticsConvs)))
    print("Retrieving full conversations. This will take a while")
    print("\t(roughly {0} seconds)\n".format(round(len(analyticsConvs)*0.4)))
    fullConvs = retrieveFullConvs(analyticsConvs)
    print("Normalizing and flattening conversation data\n")
    normalizedTables = normalizeConversations(fullConvs)
    print("Preparing to write data\n")
    writeTables(normalizedTables)
    
    
    